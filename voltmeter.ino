#include <LiquidCrystal.h>
LiquidCrystal lcd(4,5,6,7,8,9);
float input_volt=0.0;
float temp =0.0;
float r1=4700.0;
float r2=1000.0;
void setup() {
  Serial.begin(9600);
  lcd.begin(16,2);
  lcd.print("DC DIGITAL VOLTMETER");  
}

void loop() {
  int analogvalue=analogRead(A0);
  temp=(analogvalue*5.0)/1024.0;
  input_volt=temp/(r1/r1+r2);
  if(input_volt<0,1){
    input_volt=0.0;
    }
    Serial.print("V=");
    Serial.println(input_volt);
    lcd.setCursor(0,1);
    lcd.print("Voltage=");
    lcd.print("input_voltage");
    delay(300);

}
